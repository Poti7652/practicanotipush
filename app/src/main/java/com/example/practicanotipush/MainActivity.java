package com.example.practicanotipush;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practicanotipush.API.API;
import com.example.practicanotipush.Models.Peticion_Login;
import com.example.practicanotipush.Services.ServicesPeticiones;
import com.example.practicanotipush.Views.Dashboard;
import com.example.practicanotipush.Views.RegistroUsuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    TextView txtRegistro;
    EditText edtCorreo, edtPassword;
    Button btnSesion;
    Switch Sesion;
    String APITOKEN = "";
    int id;
    ServicesPeticiones service;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        service = API.getApi(MainActivity.this).create(ServicesPeticiones.class);
        verificarPreferencias();
        edtCorreo = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtContra);
        btnSesion = findViewById(R.id.btnInicio);
        txtRegistro = findViewById(R.id.txtvRegistrarU);
        Sesion = findViewById(R.id.switchSesion);
        txtRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ints = new Intent(MainActivity.this, RegistroUsuario.class);
                startActivity(ints);
            }
        });
        btnSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtCorreo.getText().toString().equals("") && edtPassword.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this,"No puede tener los campos vacios",Toast.LENGTH_LONG).show();
                }else
                    Peticion(edtCorreo.getText().toString(), edtPassword.getText().toString());
            }
        });
    }

    private void Peticion(String correo, String password){
        service.Sesion(correo, password).enqueue(new Callback<Peticion_Login>() {
            @Override
            public void onResponse(Call<Peticion_Login> call, Response<Peticion_Login> response) {
                if(response.isSuccessful()){
                    if(response.body().getEstado().equals(true)){
                        APITOKEN = response.body().getToken();
                        id = response.body().getId();
                        if(Sesion.isChecked()){
                            GuardarPreferencias();
                        }else
                            TokenTemporal();

                        Toast.makeText(MainActivity.this,"Bienvenido!!",Toast.LENGTH_LONG).show();
                        startActivity(new Intent(MainActivity.this, Dashboard.class));
                        finish();
                    }else
                        Toast.makeText(MainActivity.this,"A ocurrido un error",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Peticion_Login> call, Throwable t) {

            }
        });
    }

    public void GuardarPreferencias(){
        String token = APITOKEN;
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("TOKEN", token);
        editor.putString("ID", ""+id);
        editor.commit();
    }

    private void verificarPreferencias(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN", "");
        if(token != ""){
            Toast.makeText(MainActivity.this,"Bienvenido de Nuevo :)",Toast.LENGTH_LONG).show();
            startActivity(new Intent(MainActivity.this, Dashboard.class));
            finish();
        }
    }

    public void TokenTemporal(){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("TOKEN_TEMP", APITOKEN);
        editor.putString("ID", ""+id);
        editor.commit();
    }
}
