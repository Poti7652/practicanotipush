package com.example.practicanotipush.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practicanotipush.API.API;
import com.example.practicanotipush.Models.Peticion_Alertas;
import com.example.practicanotipush.Models.Peticion_Visualizaciones;
import com.example.practicanotipush.R;
import com.example.practicanotipush.Services.ServicesPeticiones;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Dashboard extends AppCompatActivity {

    ServicesPeticiones servicio;
    TextView txtTotalAlertas, txtvTotalVisualizaciones, txtAlertasU, txtVisuU;
    Button btnAlertsG, btnAlertUser, btnVisuG, btnVisuUsuario, btnSalir;
    int totalA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        verificarPreferencias();
        setContentView(R.layout.activity_dashboard);
        servicio = API.getApi(Dashboard.this).create(ServicesPeticiones.class);
        txtTotalAlertas = findViewById(R.id.txtvTotalAlertasG);
        txtvTotalVisualizaciones = findViewById(R.id.txtvTVisuG);
        txtAlertasU = findViewById(R.id.txtvTAlersU);
        txtVisuU = findViewById(R.id.txtvTVisuU);
        btnAlertsG = findViewById(R.id.btnAlertGeneral);
        btnAlertUser = findViewById(R.id.btnAlertUser);
        btnVisuG = findViewById(R.id.btnVisuGeneral);
        btnVisuUsuario = findViewById(R.id.btnVisuaUser);
        btnSalir =findViewById(R.id.btnCerrarSesion);
        btnAlertUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, AlertasU.class));
            }
        });
        btnAlertsG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, Alertas.class));
            }
        });
        btnVisuG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, Visualizaciones.class));

            }
        });
        btnVisuUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dashboard.this, VisualizacionU.class));
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("TOKEN_TEMP", "");
                editor.putString("TOKEN", "");
                editor.putString("ID", "");
                editor.commit();
                Toast.makeText(Dashboard.this, "Adios Vuelve Pronto :D", Toast.LENGTH_LONG).show();
                finish();
            }
        });
        CargarDatos();
    }
    private void verificarPreferencias(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        if(API.token.equals("")){
            if(!preferencias.getString("TOKEN_TEMP", "").equals("") && preferencias.getString("TOKEN", "").equals("")){
                API.token = preferencias.getString("TOKEN_TEMP", "");
            }else if(!preferencias.getString("TOKEN", "").equals("") && preferencias.getString("TOKEN_TEMP", "").equals("")){
                API.token = preferencias.getString("TOKEN", "");
            }
        }
    }

    private void CargarDatos(){
        totalAlertas();
        totalAlertasU();
        totalVisualizaciones();
        totalVisusU();
    }
    private void totalAlertas(){
        servicio.Alertas("s").enqueue(new Callback<Peticion_Alertas>() {
            @Override
            public void onResponse(Call<Peticion_Alertas> call, Response<Peticion_Alertas> response) {
                if(response.isSuccessful()){
                    txtTotalAlertas.setText(""+ response.body().getAlertas().size());
                }else
                    Log.e("Error DatosTA", "NO pude conseguir los datos");
            }

            @Override
            public void onFailure(Call<Peticion_Alertas> call, Throwable t) {
                Log.e("Error TotalA", "No me e podido conectar");
            }
        });
    }
    private void totalAlertasU(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        servicio.AlertasU(preferencias.getString("ID", "")).enqueue(new Callback<Peticion_Alertas>() {
            @Override
            public void onResponse(Call<Peticion_Alertas> call, Response<Peticion_Alertas> response) {
                if(response.isSuccessful()){
                    txtAlertasU.setText(""+ response.body().getAlertas().size());
                }else
                    Log.e("Error DatosTAU", "NO pude conseguir los datos");
            }

            @Override
            public void onFailure(Call<Peticion_Alertas> call, Throwable t) {
                Log.e("Error TotalAU", "No me e podido conectar");
            }
        });
    }
    private void totalVisualizaciones(){
        servicio.Visualizaciones("s").enqueue(new Callback<Peticion_Visualizaciones>() {
            @Override
            public void onResponse(Call<Peticion_Visualizaciones> call, Response<Peticion_Visualizaciones> response) {
                if(response.isSuccessful()){
                    txtvTotalVisualizaciones.setText(""+ response.body().getVisualizaciones().size());
                }else
                    Log.e("Error DatosTA", "NO pude conseguir los datos");
            }

            @Override
            public void onFailure(Call<Peticion_Visualizaciones> call, Throwable t) {
                Log.e("Error TotalVisu", "No me e podido conectar");
            }
        });
    }
    private void totalVisusU(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        servicio.VisualizacionesU(preferencias.getString("ID", "")).enqueue(new Callback<Peticion_Visualizaciones>() {
            @Override
            public void onResponse(Call<Peticion_Visualizaciones> call, Response<Peticion_Visualizaciones> response) {
                if(response.isSuccessful()){
                    txtVisuU.setText(""+ response.body().getVisualizaciones().size());
                }else
                    Log.e("Error DatosTAU", "NO pude conseguir los datos");
            }

            @Override
            public void onFailure(Call<Peticion_Visualizaciones> call, Throwable t) {
                Log.e("Error TotalVisuU", "No me e podido conectar");
            }
        });
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("TOKEN_TEMP", "");
        editor.putString("ID", "");
        editor.commit();
    }
}