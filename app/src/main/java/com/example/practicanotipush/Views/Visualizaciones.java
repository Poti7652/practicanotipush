package com.example.practicanotipush.Views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.practicanotipush.API.API;
import com.example.practicanotipush.Models.AdapterVisu;
import com.example.practicanotipush.Models.Peticion_Alertas;
import com.example.practicanotipush.Models.Peticion_Visualizaciones;
import com.example.practicanotipush.R;
import com.example.practicanotipush.Services.ServicesPeticiones;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Visualizaciones extends AppCompatActivity {

    ServicesPeticiones servicio;
    RecyclerView rvVisualizacones;
    AdapterVisu adapterVisu;
    Button btnRecargar;
    ImageButton imgbAtras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizaciones);
        servicio = API.getApi(Visualizaciones.this).create(ServicesPeticiones.class);
        btnRecargar = findViewById(R.id.btnRecargar);
        imgbAtras = findViewById(R.id.imgbRegresar);
        adapterVisu = new AdapterVisu();
        rvVisualizacones = findViewById(R.id.rvVisu);
        rvVisualizacones.setHasFixedSize(true);
        rvVisualizacones.setLayoutManager(new LinearLayoutManager(Visualizaciones.this));
        rvVisualizacones.addItemDecoration(new DividerItemDecoration(Visualizaciones.this, DividerItemDecoration.VERTICAL));
        TodasVisualizaciones();
        imgbAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Visualizaciones.this, Dashboard.class));
                finish();
            }
        });
        btnRecargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TodasVisualizaciones();
            }
        });
    }

    private void TodasVisualizaciones(){
        servicio.Visualizaciones("s").enqueue(new Callback<Peticion_Visualizaciones>() {
            @Override
            public void onResponse(Call<Peticion_Visualizaciones> call, Response<Peticion_Visualizaciones> response) {
                if(response.isSuccessful()){
                    List<Peticion_Visualizaciones.detalle> visualizaciones = response.body().getVisualizaciones();
                    adapterVisu.setData(visualizaciones);
                    rvVisualizacones.setAdapter(adapterVisu);
                }else{
                    Log.e("Api Error: ", response.message() + " errorBody:" + response.errorBody());
                    Toast.makeText(Visualizaciones.this, "A Ocurrido un error al procesar a los usuarios", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<Peticion_Visualizaciones> call, Throwable t) {
                Log.e("Api Error: ", t.getMessage() +" "+ t.getCause());
                Toast.makeText(Visualizaciones.this, "A Ocurrido Al conectarse con el servidor", Toast.LENGTH_LONG).show();
            }
        });
    }
}