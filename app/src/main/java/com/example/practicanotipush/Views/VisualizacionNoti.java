package com.example.practicanotipush.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.practicanotipush.API.API;
import com.example.practicanotipush.Models.Peticion_CrearAlerta;
import com.example.practicanotipush.R;
import com.example.practicanotipush.Services.ServicesPeticiones;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisualizacionNoti extends AppCompatActivity {

    ServicesPeticiones servicio;
    Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizacion_noti);
        btnSalir = findViewById(R.id.btnRegresar);
        servicio = API.getApi(VisualizacionNoti.this).create(ServicesPeticiones.class);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            CrearVisualizacion(bundle.getString("ID"));
        }else{
            Toast.makeText(VisualizacionNoti.this, "Ocurrio Un error al crear la Visualizacion", Toast.LENGTH_LONG).show();
            startActivity(new Intent(VisualizacionNoti.this, Dashboard.class));
            finish();
        }
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(VisualizacionNoti.this, Dashboard.class));
                finish();
            }
        });
    }

    private void CrearVisualizacion(String IDAlerta){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        servicio.CrearVisualziacion(preferences.getString("ID", ""), IDAlerta).enqueue(new Callback<Peticion_CrearAlerta>() {
            @Override
            public void onResponse(Call<Peticion_CrearAlerta> call, Response<Peticion_CrearAlerta> response) {
                if(!response.isSuccessful() || !response.body().isEstado()) {
                    Log.e("Error Alerta", "Ocurrio un error al crear una Visualizacion");
                    Log.e("Error Alerta", response.message());
                }else{
                    Log.e("Exito Alerta", "Se a Creado Exitosamente una Visualizacion");
                    Toast.makeText(VisualizacionNoti.this, "Gracias por Visualizar", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Peticion_CrearAlerta> call, Throwable t) {
                Log.e("Error Conexion", t.getMessage());
                Log.e("Error Conexion", "Ocurrio un error al conectar con Firebase o al servidor");
            }
        });
    }
}