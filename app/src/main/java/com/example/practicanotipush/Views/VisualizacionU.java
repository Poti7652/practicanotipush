package com.example.practicanotipush.Views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.practicanotipush.API.API;
import com.example.practicanotipush.Models.AdapterVisu;
import com.example.practicanotipush.Models.Peticion_Visualizaciones;
import com.example.practicanotipush.R;
import com.example.practicanotipush.Services.ServicesPeticiones;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisualizacionU extends AppCompatActivity {

    ServicesPeticiones servicio;
    RecyclerView rvVisualizaconesU;
    AdapterVisu adapterVisu;
    Button btnRecargar;
    ImageButton imgbAtras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizacion_u);
        servicio = API.getApi(VisualizacionU.this).create(ServicesPeticiones.class);
        btnRecargar = findViewById(R.id.btnRecargar);
        imgbAtras = findViewById(R.id.imgbRegresar);
        adapterVisu = new AdapterVisu();
        rvVisualizaconesU = findViewById(R.id.rvVisuU);
        rvVisualizaconesU.setHasFixedSize(true);
        rvVisualizaconesU.setLayoutManager(new LinearLayoutManager(VisualizacionU.this));
        rvVisualizaconesU.addItemDecoration(new DividerItemDecoration(VisualizacionU.this, DividerItemDecoration.VERTICAL));
        VisualizacionesUsuario();
        imgbAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(VisualizacionU.this, Dashboard.class));
                finish();
            }
        });
        btnRecargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VisualizacionesUsuario();
            }
        });
    }

    private void VisualizacionesUsuario(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        servicio.VisualizacionesU(preferencias.getString("ID", "")).enqueue(new Callback<Peticion_Visualizaciones>() {
            @Override
            public void onResponse(Call<Peticion_Visualizaciones> call, Response<Peticion_Visualizaciones> response) {
                if(response.isSuccessful()){
                    if(!response.body().getVisualizaciones().isEmpty() || response.body().isEstado()){
                        List<Peticion_Visualizaciones.detalle> visualizaciones = response.body().getVisualizaciones();
                        adapterVisu.setData(visualizaciones);
                        rvVisualizaconesU.setAdapter(adapterVisu);
                    }else{
                        Log.e("Api Error: ", response.message() + " errorBody:" + response.errorBody());
                        Toast.makeText(VisualizacionU.this, "No Se a Podido Encontrar datos", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Log.e("Api Error: ", response.message() + " errorBody:" + response.errorBody());
                    Toast.makeText(VisualizacionU.this, "A Ocurrido un error al procesar a los usuarios", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Peticion_Visualizaciones> call, Throwable t) {
                Log.e("Api Error: ", t.getMessage() +" "+ t.getCause());
                Toast.makeText(VisualizacionU.this, "A Ocurrido Al conectarse con el servidor", Toast.LENGTH_LONG).show();
            }
        });
    }
}