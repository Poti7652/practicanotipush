package com.example.practicanotipush.Views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practicanotipush.API.API;
import com.example.practicanotipush.MainActivity;
import com.example.practicanotipush.Models.Peticion_Registro;
import com.example.practicanotipush.R;
import com.example.practicanotipush.Services.ServicesPeticiones;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroUsuario extends AppCompatActivity {

    Button btnRegistro;
    ServicesPeticiones service;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);
        service = API.getApi(RegistroUsuario.this).create(ServicesPeticiones.class);
        btnRegistro = findViewById(R.id.btnRegistrar);
        final EditText edtCorreo = findViewById(R.id.edtEmail);
        final EditText edtContra = findViewById(R.id.edtPassword);
        final EditText edtContra2 = findViewById(R.id.edtPasswordRep);
        final TextView tvRegresar = findViewById(R.id.txtvRegresar);
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!edtContra.getText().toString().equals(edtContra2.getText().toString()))
                    Toast.makeText(RegistroUsuario.this,"Las contraseñas no Coinciden",Toast.LENGTH_LONG).show();

                if(edtCorreo.getText().toString().equals("") && edtContra.getText().toString().equals("") && edtContra2.getText().toString().equals("")){
                    Toast.makeText(RegistroUsuario.this,"Los Campos no pueden estar vacios",Toast.LENGTH_LONG).show();
                }else
                    RegistrarUsuario(edtCorreo.getText().toString(), edtContra.getText().toString());
            }
        });
        tvRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistroUsuario.this, MainActivity.class));
                finish();
            }
        });
    }
    public void RegistrarUsuario(String Correo, String Password){
        Call<Peticion_Registro> callregistro = service.registroU(Correo, Password);
        callregistro.enqueue(new Callback<Peticion_Registro>() {
            @Override
            public void onResponse(Call<Peticion_Registro> call, Response<Peticion_Registro> response) {
                Peticion_Registro peticion = response.body();
                if(response.isSuccessful()){
                    if(response.body() != null){
                        if(peticion.estado == "true"){
                            startActivity(new Intent(RegistroUsuario.this, MainActivity.class));
                            Toast.makeText(RegistroUsuario.this,"Se a Registrado Correctamente!!",Toast.LENGTH_LONG).show();
                            finish();
                        } else{
                            Toast.makeText(RegistroUsuario.this,peticion.detalle,Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(RegistroUsuario.this,"A Ocurrido algo extraño en nuestro servidor",Toast.LENGTH_SHORT).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<Peticion_Registro> call, Throwable t) {
                Toast.makeText(RegistroUsuario.this, "A Ocurrido un error al conectar con el servicio",Toast.LENGTH_LONG).show();
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }
}