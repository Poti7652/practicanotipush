package com.example.practicanotipush.Views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.practicanotipush.API.API;
import com.example.practicanotipush.Models.AdapterAlert;
import com.example.practicanotipush.Models.Peticion_Alertas;
import com.example.practicanotipush.R;
import com.example.practicanotipush.Services.ServicesPeticiones;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlertasU extends AppCompatActivity {

    ServicesPeticiones servicio;
    RecyclerView rvAlertasU;
    AdapterAlert adapterAlert;
    Button btnRecargar;
    ImageButton imgbAtras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alertas_u);
        servicio = API.getApi(AlertasU.this).create(ServicesPeticiones.class);
        adapterAlert = new AdapterAlert();
        btnRecargar = findViewById(R.id.btnRecargar);
        imgbAtras = findViewById(R.id.imgbRegresar);
        rvAlertasU = findViewById(R.id.rvAlertsU);
        rvAlertasU.setHasFixedSize(true);
        rvAlertasU.setLayoutManager(new LinearLayoutManager(AlertasU.this));
        rvAlertasU.addItemDecoration(new DividerItemDecoration(AlertasU.this, DividerItemDecoration.VERTICAL));
        AlertasUsuario();
        imgbAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AlertasU.this, Dashboard.class));
                finish();
            }
        });
        btnRecargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertasUsuario();
            }
        });
    }
    private void AlertasUsuario(){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        servicio.AlertasU(preferences.getString("ID", "")).enqueue(new Callback<Peticion_Alertas>() {
            @Override
            public void onResponse(Call<Peticion_Alertas> call, Response<Peticion_Alertas> response) {
                if (response.isSuccessful()) {
                    List<Peticion_Alertas.detalle> alertas = response.body().getAlertas();
                    if(!alertas.isEmpty()){
                        adapterAlert.setData(alertas);
                        rvAlertasU.setAdapter(adapterAlert);
                    }else
                        Toast.makeText(AlertasU.this, "No tiene Alertas el Usuario", Toast.LENGTH_LONG).show();
                } else {
                    Log.e("Api Error: ", response.message() + " errorBody:" + response.errorBody());
                    Toast.makeText(AlertasU.this, "A Ocurrido un error al procesar a los usuarios", Toast.LENGTH_LONG).show();
                    AlertasUsuario();
                }
            }

            @Override
            public void onFailure(Call<Peticion_Alertas> call, Throwable t) {
                Log.e("Api Error: ", t.getMessage() +" "+ t.getCause());
                Toast.makeText(AlertasU.this, "A Ocurrido Al conectarse con el servidor", Toast.LENGTH_LONG).show();
            }
        });
    }
}