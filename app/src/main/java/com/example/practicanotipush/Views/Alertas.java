package com.example.practicanotipush.Views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.practicanotipush.API.API;
import com.example.practicanotipush.MainActivity;
import com.example.practicanotipush.Models.AdapterAlert;
import com.example.practicanotipush.Models.Peticion_Alertas;
import com.example.practicanotipush.R;
import com.example.practicanotipush.Services.ServicesPeticiones;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Alertas extends AppCompatActivity {

    ServicesPeticiones servicio;
    RecyclerView rvAlertas;
    AdapterAlert adapterAlert;
    Button btnRecargar;
    ImageButton imgbAtras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alertas);
        servicio = API.getApi(Alertas.this).create(ServicesPeticiones.class);
        adapterAlert = new AdapterAlert();
        btnRecargar = findViewById(R.id.btnRecargar);
        imgbAtras = findViewById(R.id.imgbRegresar);
        rvAlertas = findViewById(R.id.rvAlerts);
        rvAlertas.setHasFixedSize(true);
        rvAlertas.setLayoutManager(new LinearLayoutManager(Alertas.this));
        rvAlertas.addItemDecoration(new DividerItemDecoration(Alertas.this, DividerItemDecoration.VERTICAL));
        AlertasR();
        imgbAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Alertas.this, Dashboard.class));
                finish();
            }
        });
        btnRecargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertasR();
            }
        });
    }

    public void AlertasR(){
        servicio.Alertas("s").enqueue(new Callback<Peticion_Alertas>() {
            @Override
            public void onResponse(Call<Peticion_Alertas> call, Response<Peticion_Alertas> response) {
                if (response.isSuccessful()) {
                    List<Peticion_Alertas.detalle> alertas = response.body().getAlertas();
                    adapterAlert.setData(alertas);
                    rvAlertas.setAdapter(adapterAlert);
                } else {
                    Log.e("Api Error: ", response.message() + " errorBody:" + response.errorBody());
                    Toast.makeText(Alertas.this, "A Ocurrido un error al procesar a los usuarios", Toast.LENGTH_LONG).show();
                    AlertasR();
                }
            }

            @Override
            public void onFailure(Call<Peticion_Alertas> call, Throwable t) {
                Log.e("Api Error: ", t.getMessage() +" "+ t.getCause());
                Toast.makeText(Alertas.this, "A Ocurrido Al conectarse con el servidor", Toast.LENGTH_LONG).show();
            }
        });
    }

}