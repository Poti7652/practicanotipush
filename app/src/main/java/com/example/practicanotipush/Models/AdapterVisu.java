package com.example.practicanotipush.Models;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.practicanotipush.R;

import java.util.List;

public class AdapterVisu extends RecyclerView.Adapter<AdapterVisu.ViewHolder> {

    List<Peticion_Visualizaciones.detalle> lista;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.visualizaciones_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Peticion_Visualizaciones.detalle visualizacion = lista.get(position);
        holder.txtvIdVisualizacion.setText("ID Visu: "+ visualizacion.getId());
        holder.txtvIdUsu.setText("ID Usuario: "+ visualizacion.getUsuarioId());
        holder.txtvIdAlert.setText("ID Alert: "+ visualizacion.getAlertaId());
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setData(List<Peticion_Visualizaciones.detalle> lista){
        this.lista = lista;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtvIdUsu, txtvIdVisualizacion, txtvIdAlert;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtvIdVisualizacion = itemView.findViewById(R.id.txtvIdVisu);
            txtvIdUsu = itemView.findViewById(R.id.txtvIdUsu);
            txtvIdAlert = itemView.findViewById(R.id.txtvAlertId);
        }
    }
}
