package com.example.practicanotipush.Models;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.practicanotipush.R;

import java.util.List;

public class AdapterAlert extends RecyclerView.Adapter<AdapterAlert.ViewHolder> {

    List<Peticion_Alertas.detalle> lista;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.alerts_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Peticion_Alertas.detalle alerta = lista.get(position);
        holder.txtvIdUsu.setText("Usuario ID: "+ alerta.getUsuarioId());
        holder.txtvIdAlert.setText("Alert ID: "+ alerta.getId());
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public void setData(List<Peticion_Alertas.detalle> lista){
        this.lista = lista;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtvIdUsu, txtvIdAlert;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtvIdUsu = itemView.findViewById(R.id.txtvIdUsu);
            txtvIdAlert = itemView.findViewById(R.id.txtvIdAlert);
        }
    }
}
