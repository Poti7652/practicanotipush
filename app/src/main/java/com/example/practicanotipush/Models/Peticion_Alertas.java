package com.example.practicanotipush.Models;

import java.util.ArrayList;
import java.util.List;

public class Peticion_Alertas {
    boolean estado;
    public List<detalle> alertas = new ArrayList<>();

    public List<detalle> getAlertas() {
        return alertas;
    }

    public void setAlertas(List<detalle> alertas) {
        this.alertas = alertas;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public class detalle{
        int id;
        int usuarioId;
        String created_at;
        String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUsuarioId() {
            return usuarioId;
        }

        public void setUsuarioId(int usuarioId) {
            this.usuarioId = usuarioId;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}

