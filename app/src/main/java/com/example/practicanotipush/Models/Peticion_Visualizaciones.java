package com.example.practicanotipush.Models;

import java.util.ArrayList;
import java.util.List;

public class Peticion_Visualizaciones {
    boolean estado;

    public List<detalle> visualizaciones = new ArrayList<>();

    public List<detalle> getVisualizaciones() {
        return visualizaciones;
    }

    public void setVisualizaciones(List<detalle> visualizaciones) {
        this.visualizaciones = visualizaciones;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public class detalle{
        int id;
        int usuarioId;
        int alertaId;
        String created_at;
        String updated_at;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUsuarioId() {
            return usuarioId;
        }

        public void setUsuarioId(int usuarioId) {
            this.usuarioId = usuarioId;
        }

        public int getAlertaId() {
            return alertaId;
        }

        public void setAlertaId(int alertaId) {
            this.alertaId = alertaId;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }
    }
}
