package com.example.practicanotipush.Services;

import com.example.practicanotipush.Models.Peticion_Alertas;
import com.example.practicanotipush.Models.Peticion_CrearAlerta;
import com.example.practicanotipush.Models.Peticion_Login;
import com.example.practicanotipush.Models.Peticion_Registro;
import com.example.practicanotipush.Models.Peticion_Visualizaciones;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServicesPeticiones {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Peticion_Registro> registroU(@Field("username") String IDUsuario, @Field("password") String password);

    @FormUrlEncoded
    @POST("api/login")
    Call<Peticion_Login> Sesion(@Field("username") String IDUsuario, @Field("password") String password);

    @FormUrlEncoded
    @POST("api/crearAlerta")
    Call<Peticion_CrearAlerta> CrearAlerta(@Field("usuarioId") String id);

    @FormUrlEncoded
    @POST("api/alertas")
    Call<Peticion_Alertas> Alertas(@Field("n") String s);

    @FormUrlEncoded
    @POST("api/alertasusuario")
    Call<Peticion_Alertas> AlertasU(@Field("usuarioId") String id);

    @FormUrlEncoded
    @POST("api/crearvisualizacionalerta")
    Call<Peticion_CrearAlerta> CrearVisualziacion(@Field("usuarioId") String id, @Field("alertaId") String idA);

    @FormUrlEncoded
    @POST("api/visualizaciones")
    Call<Peticion_Visualizaciones> Visualizaciones(@Field("s") String s);

    @FormUrlEncoded
    @POST("api/visualizacionesusuario")
    Call<Peticion_Visualizaciones> VisualizacionesU(@Field("usuarioId") String s);

}
