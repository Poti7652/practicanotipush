package com.example.practicanotipush.Services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.example.practicanotipush.API.API;
import com.example.practicanotipush.MainActivity;
import com.example.practicanotipush.Models.Peticion_CrearAlerta;
import com.example.practicanotipush.R;
import com.example.practicanotipush.Views.Dashboard;
import com.example.practicanotipush.Views.VisualizacionNoti;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FCM extends FirebaseMessagingService {

    ServicesPeticiones servicio;
    String Id;

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Log.e("token", "mi token es: "+s);
        guardarTokenNuevo(s);
    }

    public void guardarTokenNuevo(String s){
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("token");
        ref.child("Usuario").setValue(s);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String from = remoteMessage.getFrom();
        Log.e("TAG","Mensaje Recibido de: "+from);
        if(remoteMessage.getNotification() != null){
            Log.e("TAG","Titulo: "+remoteMessage.getNotification().getTitle());
            Log.e("TAG","Body: "+remoteMessage.getNotification().getBody());
        }
        // -- Clave valor
        if(remoteMessage.getData().size() > 0){
            Log.e("TAG","Mi titulo es "+remoteMessage.getData().get("titulo"));
            Log.e("TAG","Mi detalle es "+remoteMessage.getData().get("detalle"));
            Log.e("TAG", "ID: "+ remoteMessage.getData().get("ID"));
            CrearAlerta();
            String titulo = remoteMessage.getData().get("titulo");
            String detalle = remoteMessage.getData().get("detalle");
            Id = remoteMessage.getData().get("ID");
            if(Id == null)
                Id = "0";
            mayorQueOreo(titulo,detalle);
        }
    }

    private void mayorQueOreo(String titulo, String detalle){
        String id = "mensaje";
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, id);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel nc = new NotificationChannel(id, "nuevo", NotificationManager.IMPORTANCE_HIGH);
            nc.setShowBadge(true);
            assert  nm != null;
            nm.createNotificationChannel(nc);
        }
        builder.setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(titulo)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(detalle)
                .setContentIntent(clicnoti())
                .setContentInfo("nuevo");

        Random random = new Random();
        int idNotificacion = random.nextInt(8000);
        assert nm != null;
        nm.notify(idNotificacion, builder.build());
    }

    private PendingIntent clicnoti(){
        Intent it = new Intent(getApplicationContext(), VisualizacionNoti.class);
        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        it.putExtra("ID", Id);
        return PendingIntent.getActivity(this, 0, it, 0);
    }



    private void CrearAlerta(){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        servicio = API.getApi(getApplicationContext()).create(ServicesPeticiones.class);
        servicio.CrearAlerta(preferences.getString("ID", "")).enqueue(new Callback<Peticion_CrearAlerta>() {
            @Override
            public void onResponse(Call<Peticion_CrearAlerta> call, Response<Peticion_CrearAlerta> response) {
                if(!response.isSuccessful() || !response.body().isEstado())
                    Log.e("Error Alerta", "Ocurrio un error al crear una Alerta");
                else
                    Log.e("Exito Alerta", "Se a Creado Exitosamente una Alerta");
            }

            @Override
            public void onFailure(Call<Peticion_CrearAlerta> call, Throwable t) {
                Log.e("Error Conexion", t.getMessage());
                Log.e("Error Conexion", "Ocurrio un error al conectar con Firebase o al servidor");
            }
        });
    }
}
